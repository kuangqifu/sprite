﻿using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CK.Sprite.Form.MySql
{
    public class FormMySqlModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<FormMySqlModule>();

            Services.AddConnectionProvider(EConnectionType.MySql, new MySqlConnectionProvider());
        }
    }
}
