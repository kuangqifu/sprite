﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.MySql
{
    public class SpriteViewRepository : BaseSpriteRepository<SpriteView>, ISpriteViewRepository
    {
        public SpriteViewRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        #region Sql

        private const string SqlGetAllSpriteViewByApplicationCode = @"SELECT * FROM SpriteViews WHERE ApplicationCode=@ApplicationCode;";

        #endregion

        public List<SpriteView> GetAllSpriteViewByApplicationCode(string applicationCode)
        {
            return _unitOfWork.Connection.Query<SpriteView>(SqlGetAllSpriteViewByApplicationCode, new { ApplicationCode = applicationCode }, _unitOfWork.Transaction).ToList();
        }

        public async Task CommonChangeChildDatas<T>(Guid viewId, string tableName, string updateFieldName, string viewIdField = "ViewId", bool isOrder = false)
        {
            var strSql = $"SELECT * FROM {tableName} WHERE {viewIdField}=@ViewId{(isOrder ? " ORDER BY `Order`" : "")};";
            var childResults = (await _unitOfWork.Connection.QueryAsync<T>(strSql, new { ViewId = viewId }, _unitOfWork.Transaction)).ToList();
            var strUpdateSql = $"UPDATE SpriteViews SET {updateFieldName}=@UpdateValue,Version=@Version WHERE Id=@ViewId";
            string updateValue = "";
            if (childResults.Count > 0)
            {
                updateValue = JsonConvert.SerializeObject(childResults, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
            }
            await _unitOfWork.Connection.ExecuteAsync(strUpdateSql, new { ViewId = viewId, UpdateValue = updateValue, Version = Guid.NewGuid() }, _unitOfWork.Transaction);
        }

        public async Task ChangeRowColChildDatas(Guid viewId)
        {
            var strSqlRow = $"SELECT * FROM ItemViewRows WHERE ViewId=@ViewId;";
            var itemViewRows = (await _unitOfWork.Connection.QueryAsync<ItemViewRow>(strSqlRow, new { ViewId = viewId }, _unitOfWork.Transaction)).OrderBy(r => r.Order).ToList();
            var strSqlCol = $"SELECT * FROM ItemViewCols WHERE ViewId=@ViewId;";
            var itemViewCols = (await _unitOfWork.Connection.QueryAsync<ItemViewCol>(strSqlCol, new { ViewId = viewId }, _unitOfWork.Transaction)).OrderBy(r => r.Order).ToList();
            itemViewRows.ForEach(r =>
            {
                r.Children = itemViewCols.Where(t => t.ItemViewRowId == r.Id).ToList();
            });
            var strUpdateSql = $"UPDATE SpriteViews SET Extension1s=@UpdateValue,Version=@Version WHERE Id=@ViewId";
            string updateValue = "";
            if (itemViewRows.Count > 0)
            {
                updateValue = JsonConvert.SerializeObject(itemViewRows, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
            }
            await _unitOfWork.Connection.ExecuteAsync(strUpdateSql, new { ViewId = viewId, UpdateValue = updateValue, Version = Guid.NewGuid() }, _unitOfWork.Transaction);
        }

        public async Task ChangeRuleActionChildDatas(Guid viewId)
        {
            var strSqlRule = $"SELECT * FROM SpriteRules WHERE BusinessId=@ViewId;";
            var rules = (await _unitOfWork.Connection.QueryAsync<SpriteRule>(strSqlRule, new { ViewId = viewId }, _unitOfWork.Transaction)).ToList();
            var strSqlAction = $"SELECT * FROM RuleActions WHERE BusinessId=@ViewId;";
            var actions = (await _unitOfWork.Connection.QueryAsync<RuleAction>(strSqlAction, new { ViewId = viewId }, _unitOfWork.Transaction)).OrderBy(r => r.Order).ToList();
            rules.ForEach(r =>
            {
                r.Children = actions.Where(t => t.RuleId == r.Id).ToList();
            });
            var strUpdateSql = $"UPDATE SpriteViews SET Rules=@UpdateValue,Version=@Version WHERE Id=@ViewId";
            string updateValue = "";
            if (rules.Count > 0)
            {
                updateValue = JsonConvert.SerializeObject(rules, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
            }
            await _unitOfWork.Connection.ExecuteAsync(strUpdateSql, new { ViewId = viewId, UpdateValue = updateValue, Version = Guid.NewGuid() }, _unitOfWork.Transaction);
        }
    }
}
