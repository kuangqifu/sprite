﻿using CK.Framework.HttpApi.Host;
using CK.Sprite.Cache.Redis;
using CK.Sprite.Core;
using CK.Sprite.CrossCutting;
using CK.Sprite.Form.Core;
using CK.Sprite.Form.MySql;
using CK.Sprite.Framework;
using CK.Sprite.StackExchangeRedis;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Volo.Abp.Auditing;
using Volo.Abp.Modularity;

namespace CK.Framework
{
    public class SpriteFormModule
    {
        public static void ConfigureSpriteForm(ServiceConfigurationContext context, IConfiguration configuration)
        {
            context.Services.Configure<DefaultDbConfig>(configuration.GetSection("DefaultDbConfig"));
            context.Services.Configure<DefaultTenantStoreOptions>(configuration.GetSection("DefaultTenantStoreOptions"));
            context.Services.Configure<SpriteConfig>(configuration.GetSection("SpriteConfig"));
            context.Services.Configure<UploadFormatSettingOptions>(configuration.GetSection("UploadFormatSettingOptions"));

            context.Services.AddMvc().AddNewtonsoftJson();
            context.Services.AddModule(new FormMySqlModule()); 
            context.Services.AddModule(new FormCoreModule());
            context.Services.AddModule(new FormHttpApiModule());
            context.Services.AddModule(new BusinessModule());
            context.Services.StartApp();

            context.Services.AddSingleton<SpriteExceptionFilter, SpriteExceptionFilter>();

            context.Services.AddRedisCore();
            context.Services.AddRedisCacheSendNotice();
            //context.Services.AddLocalCacheSendNotice();
            context.Services.AddCurrentInfo();
            context.Services.AddSpriteCoreService();

            context.Services.AddMvc().AddRazorPagesOptions(o =>
            {
                o.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute());
            });

            context.Services.Configure<AbpAuditingOptions>(options =>
            {
                options.IsEnabled = false; //Disables the auditing system
            });

            context.Services.Configure<AbpAntiForgeryOptions>(options =>
            {
                options.AutoValidateIgnoredHttpMethods.Add("POST");
            });
            context.Services.Configure<MvcOptions>(mvcOptions =>
            {
                mvcOptions.AddSpriteException();
            });
        }
    }
}
