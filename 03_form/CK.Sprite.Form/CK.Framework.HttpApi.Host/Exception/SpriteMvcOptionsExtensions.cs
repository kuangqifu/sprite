﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace CK.Framework.HttpApi.Host
{
    internal static class SpriteMvcOptionsExtensions
    {
        public static void AddSpriteException(this MvcOptions options)
        {
            AddSpriteFilters(options);
        }

        private static void AddSpriteFilters(MvcOptions options)
        {
            options.Filters.AddService(typeof(SpriteExceptionFilter));
        }
    }
}