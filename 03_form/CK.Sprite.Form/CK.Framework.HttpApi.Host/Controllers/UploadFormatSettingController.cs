﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using CK.Sprite.Framework;
using CK.Sprite.ThirdContract;
using CK.Sprite.CrossCutting;
using Microsoft.Extensions.Options;

namespace CK.Framework.HttpApi.Host
{
    [ApiController]
    [Area("upload")]
    [ControllerName("UploadFormatSetting")]
    [Route("api/upload/uploadFormatSetting")]
    public class UploadFormatSettingController : Controller
    {
        private readonly UploadFormatSettingOptions _uploadFormatSettingOptions;

        public UploadFormatSettingController(IOptions<UploadFormatSettingOptions> uploadFormatSettingOptions)
        {
            _uploadFormatSettingOptions = uploadFormatSettingOptions.Value;
        }

        [HttpGet]
        [Route("GetUploadFormatSetting")]
        public async Task<UploadFormatSetting> GetUploadFormatSetting(string formateCategory = "")
        {
            if(string.IsNullOrEmpty(formateCategory))
            {
                return _uploadFormatSettingOptions.UploadFormatSettings.FirstOrDefault(r => r.FormateCategory == "Default");
            }
            else
            {
                return _uploadFormatSettingOptions.UploadFormatSettings.FirstOrDefault(r => r.FormateCategory == formateCategory);
            }
        }
    }
}
