﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace CK.Framework.Controllers
{
    public class HomeController : AbpController
    {
        public ActionResult Index()
        {
#if DEBUG
            return Redirect("/swagger");
#else
            return Redirect("/index.html");
#endif
        }
    }
}
