﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.Security;
using Volo.Abp.Threading;

namespace CK.Sprite.Form.InterceptorAbp
{
    [DependsOn(
        typeof(AbpSecurityModule)
        )]
    public class FormAbpInterceptorModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.OnRegistred(FormInterceptorRegistrar.RegisterIfNeeded);
        }
    }
}
