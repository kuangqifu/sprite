﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Business
{
    public interface IStockQtyRepository : IRepository
    {


        /// <summary>
        /// 增加仓库库存
        /// </summary>
        /// <param name="addData">新增数据实体</param>
        /// <returns></returns>
        int CheckStockQtyAsync(string stockCode, string itemCode);

        /// <summary>
        /// 新增仓库库存
        /// </summary>
        /// <param name="updateData">新增数据实体</param>
        /// <returns></returns>
        Task AddStockQtyAsync(List<string> fields, JObject addData);

        /// <summary>
        /// 修改仓库库存
        /// </summary>
        /// <param name="updateData">修改数据实体</param>
        /// <returns></returns>
        Task UpdateStockQtyAsync(string stockCode, string itemCode, decimal quantity);
    }
}
