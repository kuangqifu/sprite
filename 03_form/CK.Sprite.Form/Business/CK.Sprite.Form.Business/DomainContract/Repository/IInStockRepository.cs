﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Business
{
    public interface IInStockRepository : IRepository
    {


        /// <summary>
        /// 审核到货单
        /// </summary>
        /// <param name="arrivalNo">到货单号</param>
        /// <returns></returns>
        Task AuditArrivalAsync(string arrivalNo);


    }
}
