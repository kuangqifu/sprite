﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Business
{
    public class StockRegularService : ISingletonDependency
    {
        private AbpTimer _timer;
        private StockDomainService _stockDomainService;

        public StockRegularService(AbpTimer timer, StockDomainService stockDomainService)
        {
            _stockDomainService = stockDomainService;
            _timer = timer;
        }

        public void StartTime()
        {
            _timer.Period = 1000 * 60 * 60;
            _timer.Elapsed += _timer_Elapsed;
            _timer.RunOnStart = false;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, EventArgs e)
        {
            JObject jObject = new JObject();
            jObject["applicationCode"] = "Default";
            jObject["tenantCode"] = "Default";
            jObject["params"] = new JObject();
            if (DateTime.Now.Hour == 21 || DateTime.Now.Hour == 12)
            {
                jObject["params"]["checkTime"] = DateTime.Now.AddDays(-1);
                AsyncHelper.RunSync(() => _stockDomainService.ExecBusinessMethod(jObject));
            }

            jObject["params"]["checkTime"] = DateTime.Now;
            AsyncHelper.RunSync(() => _stockDomainService.ExecBusinessMethod(jObject));
        }
    }
}
