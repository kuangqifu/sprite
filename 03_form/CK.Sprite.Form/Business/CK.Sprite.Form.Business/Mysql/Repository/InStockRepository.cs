﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Business
{
    public class InStockRepository : IInStockRepository
    {
        public InStockRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork _unitOfWork { get; private set; }

        public async Task AuditArrivalAsync(string arrivalNo)
        {
            var strSql = $"UPDATE ArrivalMst_t SET State = '已审核'  WHERE ArrivalNo = @ArrivalNo;";
            await _unitOfWork.Connection.ExecuteAsync(strSql, new { ArrivalNo = arrivalNo });
        }
    }
}
