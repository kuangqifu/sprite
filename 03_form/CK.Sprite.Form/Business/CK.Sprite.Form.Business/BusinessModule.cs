﻿using CK.Sprite.Form.Business;
using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;

namespace CK.Sprite.Form.MySql
{
    public class BusinessModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<BusinessModule>();

            Services.AddConnectionProvider(BusinessConsts.BusinessConnProviderKey, new BusinessMySqlConnectionProvider());
        }

        public override void OnApplicationInitialization()
        {
            System.ServiceLocator.ServiceProvider.GetService<StockRegularService>().StartTime();
        }
    }
}
