﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IDictItemRepository : IBaseSpriteRepository<DictItem>
    {
        Task DeleteDictItemsByDictCode(string dictCode);
        Task<List<DictItem>> GetDictItemsByDictCode(string dictCode);
    }
}
