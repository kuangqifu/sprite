﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IObjectMethodRepository : IBaseSpriteRepository<ObjectMethod>
    {
        /// <summary>
        /// 根据applicationCode获取所有ObjectMethod信息
        /// </summary>
        /// <param name="applicationCode"></param>
        /// <returns></returns>
        List<ObjectMethod> GetAllObjectMethodByApplicationCode(string applicationCode);
    }
}
