﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteFormRepository : IBaseSpriteRepository<SpriteForm>
    {
        List<SpriteForm> GetAllSpriteFormByApplicationCode(string applicationCode);

        /// <summary>
        /// 通用修改表单字段属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewId">表单Id</param>
        /// <param name="tableName">提取数据的表名</param>
        /// <param name="updateFieldName">需要修改的字段</param>
        /// <param name="formIdField">提取表中表单Id字段名称</param>
        /// <param name="isOrder">是否排序</param>
        /// <returns></returns>
        Task CommonChangeChildDatas<T>(Guid formId, string tableName, string updateFieldName, string formIdField = "FormId", bool isOrder = false);

        /// <summary>
        /// 修改表单Item,Row,Col时，修改表单对应字段
        /// </summary>
        /// <param name="formId">表单Id</param>
        Task ChangeItemRowColChildDatas(Guid formId);

        /// <summary>
        /// 规则修改时修改表单对应字段
        /// </summary>
        /// <param name="formId">表单Id</param>
        /// <returns></returns>
        Task ChangeRuleActionChildDatas(Guid formId);
    }
}
