﻿using AutoMapper;
using CK.Sprite.CrossCutting;
using CK.Sprite.Framework;
using CK.Sprite.ThirdContract;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CK.Sprite.Form.Core
{
    public class FormCoreModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<FormCoreModule>();

            Services.AddTransient<IFormThirdServiceAppService, RuntimeAppService>();
            Services.AddSingleton<IFormViewValidator, FormValidateService>();
            Services.AddAutoMapper(typeof(AutomapperConfig));

        }
    }
}
