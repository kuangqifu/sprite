﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// View操作职责类定义
    /// </summary>
    public abstract class AbstractSpriteHandler
    {
        protected IServiceProvider _serviceProvider => ServiceLocator.ServiceProvider;
        protected readonly object ServiceProviderLock = new object();
        protected TService LazyGetRequiredService<TService>(ref TService reference)
            => LazyGetRequiredService(typeof(TService), ref reference);

        protected TRef LazyGetRequiredService<TRef>(Type serviceType, ref TRef reference)
        {
            if (reference == null)
            {
                lock (ServiceProviderLock)
                {
                    if (reference == null)
                    {
                        reference = (TRef)_serviceProvider.GetRequiredService(serviceType);
                    }
                }
            }

            return reference;
        }

        protected AbstractSpriteHandler Next;

        public void SetNext(AbstractSpriteHandler spriteViewHandler, IUnitOfWork designUnitOfWork)
        {
            Next = spriteViewHandler;
            DesignUnitOfWork = designUnitOfWork;
        }

        public DefaultDbConfig DefaultDbConfig => LazyGetRequiredService(ref _defaultDbConfig).Value;
        private IOptions<DefaultDbConfig> _defaultDbConfig;

        public IMapper Mapper => LazyGetRequiredService(ref _mapper);
        private IMapper _mapper;

        protected IUnitOfWork DesignUnitOfWork;
    }
}
