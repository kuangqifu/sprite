﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ListViewCustomerColumnHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddListViewCustomerColumn(ListViewCustomerColumn listViewCustomerColumn, GuidRepositoryBase<ListViewCustomerColumn> listViewCustomerColumnRepositoryParam = null)
        {
            var listViewCustomerColumnRepository = listViewCustomerColumnRepositoryParam == null ? new GuidRepositoryBase<ListViewCustomerColumn>(DesignUnitOfWork) : listViewCustomerColumnRepositoryParam;
            return await listViewCustomerColumnRepository.InsertAsync(listViewCustomerColumn);
        }

        public async Task<bool> UpdateListViewCustomerColumn(ListViewCustomerColumn listViewCustomerColumn, GuidRepositoryBase<ListViewCustomerColumn> listViewCustomerColumnRepositoryParam = null)
        {
            var listViewCustomerColumnRepository = listViewCustomerColumnRepositoryParam == null ? new GuidRepositoryBase<ListViewCustomerColumn>(DesignUnitOfWork) : listViewCustomerColumnRepositoryParam;
            return await listViewCustomerColumnRepository.UpdateAsync(listViewCustomerColumn);
        }

        public async Task DeleteListViewCustomerColumn(ListViewCustomerColumn listViewCustomerColumn, GuidRepositoryBase<ListViewCustomerColumn> listViewCustomerColumnRepositoryParam = null)
        {
            var listViewCustomerColumnRepository = listViewCustomerColumnRepositoryParam == null ? new GuidRepositoryBase<ListViewCustomerColumn>(DesignUnitOfWork) : listViewCustomerColumnRepositoryParam;
            await listViewCustomerColumnRepository.DeleteAsync(listViewCustomerColumn);
        }
    }
}
