﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ControlHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddControl(Control control, GuidRepositoryBase<Control> controlRepositoryParam = null)
        {
            var controlRepository = controlRepositoryParam == null ? new GuidRepositoryBase<Control>(DesignUnitOfWork) : controlRepositoryParam;
            return await controlRepository.InsertAsync(control);
        }

        public async Task<bool> UpdateControl(Control control, GuidRepositoryBase<Control> controlRepositoryParam = null)
        {
            var controlRepository = controlRepositoryParam == null ? new GuidRepositoryBase<Control>(DesignUnitOfWork) : controlRepositoryParam;
            return await controlRepository.UpdateAsync(control);
        }

        public async Task DeleteControl(Control control, GuidRepositoryBase<Control> controlRepositoryParam = null)
        {
            var controlRepository = controlRepositoryParam == null ? new GuidRepositoryBase<Control>(DesignUnitOfWork) : controlRepositoryParam;
            await controlRepository.DeleteAsync(control);
        }
    }
}
