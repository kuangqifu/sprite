﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class RuleActionHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            return await ruleActionRepository.InsertAsync(ruleAction);
        }

        public async Task<bool> UpdateRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            return await ruleActionRepository.UpdateAsync(ruleAction);
        }

        public async Task DeleteRuleAction(RuleAction ruleAction, GuidRepositoryBase<RuleAction> ruleActionRepositoryParam = null)
        {
            var ruleActionRepository = ruleActionRepositoryParam == null ? new GuidRepositoryBase<RuleAction>(DesignUnitOfWork) : ruleActionRepositoryParam;
            await ruleActionRepository.DeleteAsync(ruleAction);
        }

        public async Task DeleteSpriteRule(SpriteRule spriteRule)
        {
            var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(DesignUnitOfWork);
            await spriteCommonRepository.DeleteById("RuleActions", spriteRule.Id, "RuleId");
        }
    }
}
