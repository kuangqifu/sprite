﻿using CK.Sprite.Framework;
using System;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteFormHandler : AbstractSpriteFormHandler, ITransientDependency
    {
        public async Task ChangeWrapInfo(Guid formId, ISpriteFormRepository spriteFormRepositoryParam = null)
        {
            var spriteFormRepository = spriteFormRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(DesignUnitOfWork) : spriteFormRepositoryParam;
            await spriteFormRepository.CommonChangeChildDatas<WrapInfo>(formId, "WrapInfos", "WrapInfos", "BusinessId");
        }

        public async Task ChangeControl(Guid formId, ISpriteFormRepository spriteFormRepositoryParam = null)
        {
            var spriteFormRepository = spriteFormRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(DesignUnitOfWork) : spriteFormRepositoryParam;
            await spriteFormRepository.CommonChangeChildDatas<Control>(formId, "Controls", "Controls", "BusinessId", true);
        }

        public async Task ChangeItemRowOrCol(Guid formId, ISpriteFormRepository spriteFormRepositoryParam = null)
        {
            var spriteFormRepository = spriteFormRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(DesignUnitOfWork) : spriteFormRepositoryParam;
            await spriteFormRepository.ChangeItemRowColChildDatas(formId);
        }

        public async Task ChangeRuleOrAction(Guid formId, ISpriteFormRepository spriteFormRepositoryParam = null)
        {
            var spriteFormRepository = spriteFormRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(DesignUnitOfWork) : spriteFormRepositoryParam;
            await spriteFormRepository.ChangeRuleActionChildDatas(formId);
        }
    }
}
