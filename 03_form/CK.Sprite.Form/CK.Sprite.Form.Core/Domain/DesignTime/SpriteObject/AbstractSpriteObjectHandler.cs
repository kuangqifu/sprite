﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// Object操作职责类定义（业务表修改尽量放到放入最后一步，并且业务表的操作支持幂等性（分布式事物影响））
    /// </summary>
    public abstract class AbstractSpriteObjectHandler : AbstractSpriteHandler
    {
        /// <summary>
        /// 新增SpriteObject
        /// </summary>
        /// <param name="spriteObjectDto">经过计算Id和ApplicationCode之后的实体</param>
        /// <returns></returns>
        public async Task AddSpriteObjectAsync(SpriteObjectDto spriteObjectDto)
        {
            await DoAddSpriteObjectAsync(spriteObjectDto);
            if (Next != null)
            {
                await (Next as AbstractSpriteObjectHandler).AddSpriteObjectAsync(spriteObjectDto);
            }
        }

        #region virtual

        protected virtual Task DoAddSpriteObjectAsync(SpriteObjectDto spriteObjectDto)
        {
            return Task.CompletedTask;
        }

        #endregion
    }
}
