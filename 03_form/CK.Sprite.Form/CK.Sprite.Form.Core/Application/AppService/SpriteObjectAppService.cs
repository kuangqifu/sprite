﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Authorize]
    public class SpriteObjectAppService : AppService, ISpriteObjectAppService
    {
        private readonly SpriteObjectService _spriteObjectService;
        public CommonService _commonService => LazyGetRequiredService(ref commonService);
        private CommonService commonService;
        public SpriteObjectAppService(SpriteObjectService spriteObjectService)
        {
            _spriteObjectService = spriteObjectService;
        }

        public async Task AddSpriteObjectAsync(SpriteObjectCreateDto spriteObjectCreateDto)
        {
            await _spriteObjectService.AddSpriteObjectAsync(spriteObjectCreateDto);
        }

        public async Task UpdateSpriteObject(SpriteObjectUpdateDto spriteObjectUpdateDto)
        {
            await _spriteObjectService.UpdateSpriteObject(spriteObjectUpdateDto);
        }

        public async Task<PageResultDto<SpriteObjectDto>> GetListSpriteObjectAsync(GetListSpriteObjectInput input)
        {
            var domainResult = await _spriteObjectService.GetListSpriteObjectAsync(input);
            var result = new PageResultDto<SpriteObjectDto>();
            result.TotalCount = domainResult.TotalCount;
            result.Items = _mapper.Map<List<SpriteObjectDto>>(domainResult.Items);

            return result;
        }

        public async Task<SpriteObjectDto> GetSpriteObjectByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<SpriteObject>("SpriteObjects", id);
            return _mapper.Map<SpriteObjectDto>(dbData);
        }

        public async Task<List<string>> GetCategorysAsync()
        {
            return await _spriteObjectService.GetCategorysAsync();
        }

        #region ObjectProperty Operate

        public async Task AddObjectProperty(ObjectPropertyCreateDto objectPropertyCreateDto)
        {
            await _spriteObjectService.AddObjectProperty(objectPropertyCreateDto);
        }

        public async Task UpdateObjectProperty(ObjectPropertyUpdateDto objectPropertyUpdateDto)
        {
            await _spriteObjectService.UpdateObjectProperty(objectPropertyUpdateDto);
        }

        public async Task DeleteObjectProperty(Guid id)
        {
            await _spriteObjectService.DeleteObjectProperty(id);
        }

        public async Task<List<ObjectPropertyDto>> GetListObjectPropertyAsync(Guid objectId)
        {
            var objectPropertys = await _spriteObjectService.GetListObjectPropertyAsync(objectId);
            return _mapper.Map<List<ObjectPropertyDto>>(objectPropertys);
        }

        public async Task<ObjectPropertyDto> GetObjectPropertyByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ObjectProperty>("ObjectPropertys", id);
            return _mapper.Map<ObjectPropertyDto>(dbData);
        }

        #endregion

        #region ObjectMethod Operate

        public async Task AddObjectMethod(ObjectMethodCreateDto objectMethodCreateDto)
        {
            await _spriteObjectService.AddObjectMethodAsync(objectMethodCreateDto);
        }

        public async Task UpdateObjectMethod(ObjectMethodUpdateDto objectMethodUpdateDto)
        {
            await _spriteObjectService.UpdateObjectMethodAsync(objectMethodUpdateDto);
        }

        public async Task DeleteObjectMethod(Guid id)
        {
            await _spriteObjectService.DeleteObjectMethod(id);
        }

        public async Task<List<ObjectMethodDto>> GetListObjectMethodAsync(Guid objectId)
        {
            var results = await _spriteObjectService.GetListObjectMethodAsync(objectId);
            return _mapper.Map<List<ObjectMethodDto>>(results);
        }

        public async Task<ObjectMethodDto> GetObjectMethodByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ObjectMethod>("ObjectMethods", id);
            return _mapper.Map<ObjectMethodDto>(dbData);
        }

        #endregion
    }
}
