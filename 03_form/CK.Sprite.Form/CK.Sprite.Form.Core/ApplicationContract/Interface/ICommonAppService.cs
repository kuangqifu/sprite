﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ICommonAppService : IAppService
    {
        #region WrapInfo Operate

        Task AddWrapInfo(WrapInfoCreateDto wrapInfoCreateDto);

        Task UpdateWrapInfo(WrapInfoUpdateDto wrapInfoUpdateDto);

        Task DeleteWrapInfo(Guid id);

        Task<List<WrapInfoDto>> GetListWrapInfoAsync(Guid businessId);

        Task<WrapInfoDto> GetWrapInfoByIdAsync(Guid id);

        #endregion

        #region Control Operate

        Task AddControl(ControlCreateDto controlCreateDto);

        Task UpdateControl(ControlUpdateDto controlUpdateDto);

        Task DeleteControl(Guid id);

        Task<List<ControlDto>> GetListControlAsync(Guid businessId);

        Task<ControlDto> GetControlByIdAsync(Guid id);

        #endregion

        #region SpriteRule Operate

        Task AddSpriteRule(SpriteRuleCreateDto spriteRuleCreateDto);

        Task UpdateSpriteRule(SpriteRuleUpdateDto spriteRuleUpdateDto);

        Task DeleteSpriteRule(Guid id);

        Task<List<SpriteRuleDto>> GetListSpriteRuleAsync(Guid businessId);

        Task<SpriteRuleDto> GetSpriteRuleByIdAsync(Guid id);

        #endregion

        #region RuleAction Operate

        Task AddRuleAction(RuleActionCreateDto ruleActionCreateDto);

        Task UpdateRuleAction(RuleActionUpdateDto ruleActionUpdateDto);

        Task DeleteRuleAction(Guid id);

        Task<List<RuleActionDto>> GetListRuleActionAsync(Guid ruleId);

        Task<RuleActionDto> GetRuleActionByIdAsync(Guid id);

        #endregion
    }
}
