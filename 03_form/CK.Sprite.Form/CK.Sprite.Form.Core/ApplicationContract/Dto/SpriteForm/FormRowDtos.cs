﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class FormRowDto : FormRowUpdateDto
    {
        /// <summary>
        /// 表单Id
        /// </summary>
        public Guid FormId { get; set; }

        /// <summary>
        /// 表单项Id
        /// </summary>
        public Guid FormItemId { get; set; }
    }

    [Serializable]
    public class FormRowCreateDto : FormRowUpdateDto
    {
        /// <summary>
        /// 表单Id
        /// </summary>
        public Guid FormId { get; set; }

        /// <summary>
        /// 表单项Id
        /// </summary>
        public Guid FormItemId { get; set; }
    }

    [Serializable]
    public class FormRowUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 表单行属性设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }

    }
}
