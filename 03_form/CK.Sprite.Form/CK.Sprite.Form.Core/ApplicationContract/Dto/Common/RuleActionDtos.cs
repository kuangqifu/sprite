﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class RuleActionDto : RuleActionUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// 规则Id
        /// </summary>
        public Guid RuleId { get; set; }
    }

    [Serializable]
    public class RuleActionCreateDto : RuleActionUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// 规则Id
        /// </summary>
        public Guid RuleId { get; set; }
    }

    [Serializable]
    public class RuleActionUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 子表单Id
        /// </summary>
        public Guid? SubFormId { get; set; }

        /// <summary>
        /// 子视图Id
        /// </summary>
        public Guid? SubViewId { get; set; }

        /// <summary>
        /// 控件Id(Modal等Wrap对象为控件Id+特定属性标识)
        /// </summary>
        public string ObjId { get; set; }

        /// <summary>
        /// 设置属性=1,执行方法=2,绑定数据=3,条件判断=4,停止执行=5,弹出确认=6,执行其他规则=7
        /// </summary>
        public EActionType ActionType { get; set; }

        /// <summary>
        /// 执行配置
        /// </summary>
        public string ActionConfig { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }

    }
}
