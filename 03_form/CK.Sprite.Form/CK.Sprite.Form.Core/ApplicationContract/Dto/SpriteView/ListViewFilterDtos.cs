﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class ListViewFilterDto : ListViewFilterUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ListViewFilterCreateDto : ListViewFilterUpdateDto
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }
    }

    [Serializable]
    public class ListViewFilterUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 隐藏查询条件=1,常规查询=2,高级查询=3
        /// </summary>
        public EFilterType FilterType { get; set; }

        /// <summary>
        /// 绑定object属性字段
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 允许查询条件类型（拼接查询条件枚举，通过‘;’隔开）
        /// </summary>
        public string AllowConditionTypes { get; set; }

        /// <summary>
        /// Label附加属性
        /// </summary>
        public string LabelPropertySettings { get; set; }

        /// <summary>
        /// 查询控件，TextBox,SingleDropDown,SingleDropDownAndTextBox,MultipleDropDown,MultipleDropDownAndTextBox,SingleTile,MultipleTile,DateTime,Radio,Checkbox
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 列自定义组件设置
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 默认查询条件
        /// </summary>
        public EConditionType DefaultConditionType { get; set; }

        /// <summary>
        /// 调用方法Url或者方法Id
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Label显示值
        /// </summary>
        public string LabelValue { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }
    }
}
