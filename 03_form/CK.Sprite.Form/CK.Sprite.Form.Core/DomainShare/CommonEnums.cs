﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 输入输出类型
    /// </summary>
    public enum EParamDirection
    {
        In = 1,
        Out = 2
    }

    /// <summary>
    /// 参数值类型
    /// </summary>
    public enum EParamType
    {
        Single = 1,
        Object = 2,
        /// <summary>
        /// (Dictionary也用List表示,名字为Key和Value)
        /// </summary>
        List = 3,
        Array = 4,
        Tree = 5
    }

    /// <summary>
    /// Sql映射条件
    /// </summary>
    public enum EConditionType
    {
        等于 = 1,
        不等于 = 2,
        Between = 3,
        In = 4,
        Like = 5,
        大于 = 6,
        大于等于 = 7,
        小于 = 8,
        小于等于 = 9,
        Null = 10,
        NotNull = 11,
        NotIn = 12
    }

    /// <summary>
    /// 验证枚举
    /// </summary>
    public enum EValidateType
    {
        Phone = 1,
        Email = 2
    }

    /// <summary>
    /// 列类型
    /// </summary>
    public enum EColType
    {
        Control = 1,
        View = 2,
        Form = 3
    }

    /// <summary>
    /// Control操作类型
    /// </summary>
    public enum EOperateType
    {
        ListView视图方法执行=1,
        ListView查询按钮 = 2,
        ListView操作方法执行 = 3,
        ListView列表更多方法执行 = 4,
    }

    /// <summary>
    /// 规则执行类型
    /// </summary>
    public enum EActionType
    {
        设置属性 = 1,
        执行方法 = 2,
        绑定数据 = 3,
        条件判断 = 4,
        停止执行 = 5,
        弹出确认 = 6,
        执行其他规则 = 7,
        重新加载页面 = 8
    }
}