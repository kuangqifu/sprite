﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class CommonConsts
    {
        public const string SpriteObjectCacheKey = "SpriteObject_Cache";
        public const string ObjectMethodCacheKey = "ObjectMethod_Cache";
        public const string SpriteViewCacheKey = "SpriteView_Cache";
        public const string SpriteFormCacheKey = "SpriteForm_Cache";

        public const string FrameworkDictCacheKey = "Framework_Dict_Cache";

        public const string SpriteViewTableName = "SpriteViews";
        public const string SpriteObjectTableName = "SpriteObjects";
        public const string SpriteFormTableName = "SpriteForms";
        public const string ObjectMethodTableName = "ObjectMethods";

        public const string SpriteFormCachePreKey = "SpriteForm";


        public static List<string> SqlExcludeStrings = new List<string>() { "select", "insert", "delete", "drop", "update", "truncate", "--", "#", "@", ";", "/*", "//" };

        public static void CheckSqlInject(string originString, List<string> excludes = null)
        {
            if (string.IsNullOrEmpty(originString))
            {
                return;
            }
            bool isInject = false;
            if (excludes != null && excludes.Count > 0)
            {
                List<string> sqlExcludeStrings = CommonConsts.SqlExcludeStrings.Where(r => !excludes.Contains(r)).ToList();
                isInject = sqlExcludeStrings.Any(r => originString.ToLower().Contains(r));
            }
            else
            {
                isInject = CommonConsts.SqlExcludeStrings.Any(r => originString.ToLower().Contains(r));
            }
            if (isInject)
            {
                throw new SpriteException("不能包含特殊字符");
            }
        }
    }
}
