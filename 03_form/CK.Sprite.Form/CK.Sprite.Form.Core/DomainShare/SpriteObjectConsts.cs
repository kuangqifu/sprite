﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteObjectConsts
    {
        public const string DefaultCreateMethodName = "Create";
        public const string DefaultUpdateMethodName = "Update";
        public const string DefaultCreateOrUpdateMethodName = "CreateOrUpdate";
        public const string DefaultGetMethodName = "Get";
        public const string DefaultFact = "Fact";
        public const string DefaultDeleteMethodName = "Delete";
        public const string DefaultListMethodName = "List";
        public const string DefaultCreateRangeMethodName = "CreateRange";
        public const string DefaultUpdateWhereMethodName = "UpdateWhere";
        public const string DefaultGetWhereMethodName = "GetWhere";
        public const string DefaultDeleteWhereMethodName = "DeleteWhere";
        public const string DefaultListWhereMethodName = "ListWhere";
        public const string MultiListWhereMethodName = "MultiListWhere";
        public const string DefaultTreeListWhereMethodName = "TreeListWhere";
        public const string DefaultPageListMethodName = "PageList";
        public const string MultiPageListMethodName = "MultiPageList";
        public const string BatchCreateName = "BatchCreate";

        public const string FormateTimeMethodName = "FormateTime";
        public const string CurrentUserInfoMethodName = "CurrentUserInfo";
        public const string CurrentUserDeptInfoMethodName = "CurrentUserDeptInfo";
        public const string UserInfoMethodName = "UserInfo";
        public const string UserInfoDeptMethodName = "UserInfoDept";

        public const string Workflow_ParentFormInfo_FormId = "Workflow_ParentFormInfo_FormId"; // 获取父流程表单信息
        public const string Workflow_OpenWorkflowData_FormId = "Workflow_OpenWorkflowData_FormId"; // 获取工作流信息
        public const string Workflow_ParentOpenWorkflowData_FormId = "Workflow_ParentOpenWorkflowData_FormId"; // 获取父流程工作流信息
        public const string Workflow_ParentFormInfo_InstanceId = "Workflow_ParentFormInfo_InstanceId"; // 获取父流程表单信息
        public const string Workflow_OpenWorkflowData_InstanceId = "Workflow_OpenWorkflowData_InstanceId"; // 获取工作流信息
        public const string Workflow_ParentOpenWorkflowData_InstanceId = "Workflow_ParentOpenWorkflowData_InstanceId"; // 获取父流程工作流信息
    }
}
