﻿using CK.Sprite.CrossCutting;
using CK.Sprite.Interceptor;
using System;
using System.Linq;

namespace CK.Sprite.Form.Interceptor
{
    public static class FormInterceptorRegistrar
    {
        public static void RegisterIfNeeded(IOnServiceRegistredContext context)
        {
            if (ShouldIntercept(context.ImplementationType))
            {
                context.Interceptors.TryAdd<FormInterceptor>();
            }
        }

        private static bool ShouldIntercept(Type type)
        {
            if (typeof(IFormInterceptEnabled).IsAssignableFrom(type)) // 只要实现了IApplicationService接口的自动注册
            {
                return true;
            }

            if (ShouldAuditTypeByDefault(type))
            {
                return true;
            }

            if (type.GetMethods().Any(m => m.IsDefined(typeof(FormInterceptAttribute), true)))
            {
                return true;
            }

            return false;
        }

        //TODO: Move to a better place
        public static bool ShouldAuditTypeByDefault(Type type)
        {
            //TODO: In an inheritance chain, it would be better to check the attributes on the top class first.

            if (type.IsDefined(typeof(FormInterceptAttribute), true))
            {
                return true;
            }

            if (type.IsDefined(typeof(FormInterceptAttribute), true))
            {
                return false;
            }

            if (typeof(IFormInterceptEnabled).IsAssignableFrom(type))
            {
                return true;
            }

            return false;
        }
    }
}