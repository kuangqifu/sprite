﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using CK.Sprite.Framework;

namespace CK.Sprite.Interceptor
{
    public class InterceptorModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddTransient(typeof(AbpAsyncDeterminationInterceptor<>));
            Services.AddAssemblyOf<InterceptorModule>();
        }
    }
}
