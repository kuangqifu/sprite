﻿using System.Threading.Tasks;

namespace CK.Sprite.Interceptor
{
    /// <summary>
    /// Interface Or public virtual method
    /// </summary>
	public interface IAbpInterceptor
    {
        Task InterceptAsync(IAbpMethodInvocation invocation);
	}
}
