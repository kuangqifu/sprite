﻿using Autofac;
using Microsoft.Extensions.DependencyInjection;
using CK.Sprite.Interceptor;

namespace Microsoft.Extensions.Hosting
{
    public static class AbpAutofacHostBuilderExtensions
    {
        public static IHostBuilder UseSpriteAutofac(this IHostBuilder hostBuilder)
        {
            var containerBuilder = new ContainerBuilder();

            return hostBuilder.ConfigureServices((_, services) =>
                {
                    services.AddObjectAccessor(containerBuilder);
                })
                .UseServiceProviderFactory(new AbpAutofacServiceProviderFactory(containerBuilder));
        }
    }
}
