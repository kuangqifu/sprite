﻿using System;
using Autofac;
using CK.Sprite.Interceptor;
using CK.Sprite.Framework;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AbpAutofacServiceCollectionExtensions
    {
        public static ContainerBuilder GetContainerBuilder(this IServiceCollection services)
        {
            var builder = services.GetObjectOrNull<ContainerBuilder>();
            if (builder == null)
            {
                throw new SpriteException($"Could not find ContainerBuilder. Be sure that you have called {nameof(AbpAutofacAbpApplicationCreationOptionsExtensions.UseAutofac)} method before!");
            }

            return builder;
        }

        public static IServiceProvider BuildAutofacServiceProvider(this IServiceCollection services, Action<ContainerBuilder> builderAction = null)
        {
	        return services.BuildServiceProviderFromFactory(builderAction);
        }
	}
}
