﻿using Autofac;
using Microsoft.Extensions.DependencyInjection;

namespace CK.Sprite.Interceptor
{
    public static class AbpAutofacAbpApplicationCreationOptionsExtensions
    {
        public static void UseAutofac(this AbpApplicationCreationOptions options)
        {
            var builder = new ContainerBuilder();
            options.Services.AddObjectAccessor(builder);
            options.Services.AddSingleton((IServiceProviderFactory<ContainerBuilder>) new AbpAutofacServiceProviderFactory(builder));
        }
    }
}
