﻿using Castle.DynamicProxy;

namespace CK.Sprite.Interceptor
{
    public class AbpAsyncDeterminationInterceptor<TInterceptor> : AsyncDeterminationInterceptor
        where TInterceptor : IAbpInterceptor
    {
        public AbpAsyncDeterminationInterceptor(TInterceptor abpInterceptor)
            : base(new CastleAsyncAbpInterceptorAdapter<TInterceptor>(abpInterceptor))
        {

        }
    }
}