﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }

    public class GuidEntityBase : IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseCreationAudited : CreationAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseModificationAudited : ModificationAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseDeletionAudited : DeletionAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseCreationModificationAudited : CreationModificationAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseCreationDeletionAudited : CreationDeletionAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseModificationDeletionAudited : ModificationDeletionAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }

    public class GuidEntityBaseFullAudited : FullAudited, IEntity<Guid>
    {
        [Dapper.Contrib.Extensions.ExplicitKey]
        public Guid Id { get; set; }
    }
}
