﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public interface ICreationAudited
    {
        string CreatorId { get; set; }
        DateTime? CreationTime { get; set; }
    }

    public class CreationAudited : ICreationAudited
    {
        [JsonIgnore]
        public string CreatorId { get; set; }
        [JsonIgnore]
        public DateTime? CreationTime { get; set; }
    }
}
