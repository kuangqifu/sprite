﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public interface IModificationAudited
    {
        string LastModifierId { get; set; }
        DateTime? LastModificationTime { get; set; }
    }

    public class ModificationAudited : IModificationAudited
    {
        [JsonIgnore]
        public string LastModifierId { get; set; }
        [JsonIgnore]
        public DateTime? LastModificationTime { get; set; }
    }
}
