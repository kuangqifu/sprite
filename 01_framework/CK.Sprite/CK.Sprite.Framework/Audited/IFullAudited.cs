﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public interface IFullAudited : ICreationAudited, IModificationAudited, IDeletionAudited
    {
    }

    public class CreationModificationAudited : CreationAudited, IModificationAudited
    {
        [JsonIgnore]
        public string LastModifierId { get; set; }
        [JsonIgnore]
        public DateTime? LastModificationTime { get; set; }
    }

    public class CreationDeletionAudited : CreationAudited, IDeletionAudited
    {
        [JsonIgnore]
        public bool? IsDeleted { get; set; }
        [JsonIgnore]
        public string DeleterId { get; set; }
        [JsonIgnore]
        public DateTime? DeletionTime { get; set; }
    }

    public class ModificationDeletionAudited : ModificationAudited, IDeletionAudited
    {
        [JsonIgnore]
        public bool? IsDeleted { get; set; }
        [JsonIgnore]
        public string DeleterId { get; set; }
        [JsonIgnore]
        public DateTime? DeletionTime { get; set; }
    }

    public class FullAudited : CreationModificationAudited, IFullAudited
    {
        [JsonIgnore]
        public bool? IsDeleted { get; set; }
        [JsonIgnore]
        public string DeleterId { get; set; }
        [JsonIgnore]
        public DateTime? DeletionTime { get; set; }
    }
}
