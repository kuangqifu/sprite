﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public interface IDeletionAudited
    {
        bool? IsDeleted { get; set; }
        string DeleterId { get; set; }
        DateTime? DeletionTime { get; set; }
    }

    public class DeletionAudited : IDeletionAudited
    {
        [JsonIgnore]
        public bool? IsDeleted { get; set; }
        [JsonIgnore]
        public string DeleterId { get; set; }
        [JsonIgnore]
        public DateTime? DeletionTime { get; set; }
    }
}
