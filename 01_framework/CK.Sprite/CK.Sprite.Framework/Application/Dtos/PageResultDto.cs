﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.Framework
{
    public class PageResultDto<T>
    {
        public List<T> Items { get; set; }
        public long TotalCount { get; set; }
    }
}
