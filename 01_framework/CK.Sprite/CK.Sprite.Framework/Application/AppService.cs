﻿using AutoMapper;
using CK.Sprite.ThirdContract;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Framework
{
    public abstract class AppService : IAppService, ITransientDependency
    {
        protected IServiceProvider _serviceProvider => ServiceLocator.ServiceProvider;
        protected readonly object ServiceProviderLock = new object();
        public AppService()
        {
        }

        protected TService LazyGetRequiredService<TService>(ref TService reference)
            => LazyGetRequiredService(typeof(TService), ref reference);

        protected TRef LazyGetRequiredService<TRef>(Type serviceType, ref TRef reference)
        {
            if (reference == null)
            {
                lock (ServiceProviderLock)
                {
                    if (reference == null)
                    {
                        reference = (TRef)_serviceProvider.GetRequiredService(serviceType);
                    }
                }
            }

            return reference;
        }

        public IMapper _mapper => LazyGetRequiredService(ref mapper);
        private IMapper mapper;

        public ICurrentUser _currentUser => LazyGetRequiredService(ref currentUser);
        private ICurrentUser currentUser;
    }
}
