﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CK.Sprite.Core
{
    public static class SpriteCoreServiceExtensions
    {
        public static IServiceCollection AddSpriteCoreService(this IServiceCollection services)
        {
            services.AddSingleton(typeof(ISerialNumberService), typeof(RedisSerialNumberService));
            return services;
        }
    }
}
