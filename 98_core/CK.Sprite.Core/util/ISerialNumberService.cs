﻿using Jint;
using Jint.Native;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CK.Sprite.Core
{
    public interface ISerialNumberService
    {
        /// <summary>
        /// Redis生成流水号
        /// </summary>
        /// <param name="serialNumberInfo">流水号信息</param>
        /// <param name="redisKey">格式：表名-字段名称</param>
        /// <param name="length">生成多少个流水号，默认为1</param>
        /// <returns>注意：生成的流水号数组是按照从大到小排序的</returns>
        Task<List<string>> CreateSerialNumber(SerialNumberInfo serialNumberInfo, string redisKey, int length = 1);
    }

    public class SerialNumberInfo
    {
        public string preNo { get; set; }
        public int length { get; set; }
        public bool useYear { get; set; }
        public bool useMonth { get; set; }
        public bool useDay { get; set; }
    }
}
