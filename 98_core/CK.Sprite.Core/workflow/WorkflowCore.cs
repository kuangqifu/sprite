﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace CK.Sprite.Core
{
    public class WorkflowCore
    {
        public void SetCallFormData(JArray methodInfos, DateTime currentTime, string addUserId)
        {
            foreach (var methodInfo in methodInfos)
            {
                if (methodInfo["datas"] != null && methodInfo["datas"]["paramValues"] != null && methodInfo["datas"]["paramValues"]["instanceId"] != null)
                {
                    (methodInfo["datas"]["paramValues"] as JObject).Add(new JProperty("flowStartTime", currentTime));
                    (methodInfo["datas"]["paramValues"] as JObject).Add(new JProperty("flowStartUserId", addUserId));
                }
            }
        }

        public void SetCallFormData2(JArray methodInfos, JArray callResults, out string formRuleId)
        {
            formRuleId = "";
            var factRuleIds = new List<string>();
            foreach (var methodInfo in methodInfos)
            {
                if (methodInfo["execType"] != null && methodInfo["execType"].ToString() == "workflow_formId")
                {
                    formRuleId = methodInfo["ruleId"].ToString();
                }

                if (methodInfo["execType"] != null && methodInfo["execType"].ToString() == "workflow_fact")
                {
                    factRuleIds.Add(methodInfo["ruleId"].ToString());
                }
            }
        }
    }
}
