﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CK.Sprite.Core
{
    public interface IWorkflowThirdService
    {
        /// <summary>
        /// 获取表单关联流程信息
        /// </summary>
        /// <param name="instanceIds">流程实例Id集合</param>
        /// <returns></returns>
        Task<List<FormWorkflowInfo>> GetFormWorkflowInfos(List<string> instanceIds);
    }
}
