﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using System;
using System.Collections.Generic;
using Aliyun.OSS.NetCore.Common.Communication;
using Aliyun.OSS.NetCore.Util;
using Aliyun.OSS.NetCore.Transform;

namespace Aliyun.OSS.NetCore.Commands
{
    internal class GetBucketLifecycleCommand : OssCommand<IList<LifecycleRule>>
    {
        private readonly string _bucketName;

        protected override string Bucket
        {
            get { return _bucketName; }
        }

        private GetBucketLifecycleCommand(IServiceClient client, Uri endpoint, ExecutionContext context,
                                    string bucketName, IDeserializer<ServiceResponse, IList<LifecycleRule>> deserializer)
            : base(client, endpoint, context, deserializer)
        {
            OssUtils.CheckBucketName(bucketName);
            _bucketName = bucketName;
        }

        public static GetBucketLifecycleCommand Create(IServiceClient client, Uri endpoint,
                                                      ExecutionContext context,
                                                      string bucketName)
        {
            return new GetBucketLifecycleCommand(client, endpoint, context, bucketName,
                                           DeserializerFactory.GetFactory().CreateGetBucketLifecycleDeserializer());
        }

        protected override IDictionary<string, string> Parameters
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { RequestParameters.SUBRESOURCE_LIFECYCLE, null }
                };
            }
        }
    }
}
