﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

namespace Aliyun.OSS.NetCore
{
    /// <summary>
    /// Permission enum definition
    /// </summary>
    public enum Permission {
        /// <summary>
        /// read only
        /// </summary>
        Read = 0,

        /// <summary>
        /// ful control
        /// </summary>
        FullControl
    }

}
