﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */
using Aliyun.OSS.NetCore.Model;

namespace Aliyun.OSS.NetCore
{    /// <summary>
     /// The result class of the operation to get bucket's policy.
     /// </summary>
    public class GetBucketPolicyResult :GenericResult
    {
        /// <summary>
        /// The bucket's policy.
        /// </summary>
        public string Policy { get; internal set; }
    }
}
