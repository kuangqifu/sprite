﻿/*
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 * 
 */

using Aliyun.OSS.NetCore.Model;
namespace Aliyun.OSS.NetCore
{
    /// <summary>
    /// The result class of the operation to get bucket's versioning configuration.
    /// </summary>
    public class GetBucketVersioningResult : GenericResult
    {
        /// <summary>
        /// Gets the versioning status
        /// </summary>
        public VersioningStatus Status { get; set; }
    }
}
