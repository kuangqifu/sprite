﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace System
{
    public class ServiceLocator
    {
        public static IServiceProvider ServiceProvider { get; private set; }
        public static void SetServices(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }
    }
}
