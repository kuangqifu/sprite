﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CK.Sprite.CrossCutting
{
    public enum ETimeType
    {
        今天 = 1,
        本周 = 2,
        本月 = 3,
        本年 = 4,
        本周到今天 = 5,
        本月到今天 = 6,
        本年到今天 = 7,
        昨天 = 31,
        前天 = 32,
        前一周 = 33,
        上周 = 34,
        前一月 = 35,
        上月 = 36,
        前一年 = 37,
        去年 = 38,
        前年 = 39,
        前三天 = 40,
        明天 = 51,
        后天 = 52,
        下一周 = 53,
        下周 = 54,
        下一月 = 55,
        下月 = 56,
        下一年 = 57,
        明年 = 58,
        后年 = 59,
        后三天 = 60,
        所有 = 100,
        用户指定 = 101,
        更早 = 102,
        更晚 = 103,
    }

    /// <summary>
    /// 计算时间帮助类
    /// </summary>
    public class CalculateTimeHelper
    {
        /// <summary>
        /// 获取查询开始时间和结束时间
        /// </summary>
        /// <param name="timeType">类型</param>
        /// <param name="calculateDate">计算的所在日期，为空，则计算当天</param>
        /// <param name="startTime">计算后的开始时间</param>
        /// <param name="endTime">计算后的结束时间</param>
        public static void CalculateDateTime(ETimeType timeType,DateTime? calculateDate, ref DateTime startTime, ref DateTime endTime)
        {
            DateTime date = calculateDate.HasValue? calculateDate.Value: DateTime.Today;
            var day = date.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)date.DayOfWeek;
            switch (timeType)
            {
                case ETimeType.今天:
                    startTime = date;
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.本周:
                    startTime = date.AddDays(1 - day);
                    endTime = startTime.AddDays(7).AddMilliseconds(-1);
                    break;
                case ETimeType.本月:
                    startTime = new DateTime(date.Year, date.Month, 1);
                    endTime = startTime.AddMonths(1).AddMilliseconds(-1);
                    break;
                case ETimeType.本年:
                    startTime = new DateTime(date.Year, 1, 1);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.本周到今天:
                    startTime = date.AddDays(1 - day);
                    endTime = date.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.本月到今天:
                    startTime = new DateTime(date.Year, date.Month, 1);
                    endTime = date.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.本年到今天:
                    startTime = new DateTime(date.Year, 1, 1);
                    endTime = date.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.昨天:
                    startTime = date.AddDays(-1);
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.前天:
                    startTime = date.AddDays(-2);
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.前一周:
                    startTime = date.AddDays(-7);
                    endTime = date.AddMilliseconds(-1);
                    break;
                case ETimeType.上周:
                    startTime = date.AddDays(1 - day - 7);
                    endTime = startTime.AddDays(7).AddMilliseconds(-1);
                    break;
                case ETimeType.前一月:
                    startTime = date.AddMonths(-1);
                    endTime = date.AddMilliseconds(-1);
                    break;
                case ETimeType.上月:
                    startTime = new DateTime(date.Year, date.Month, 1).AddMonths(-1);
                    endTime = startTime.AddMonths(1).AddMilliseconds(-1);
                    break;
                case ETimeType.前一年:
                    startTime = date.AddYears(-1);
                    endTime = date.AddMilliseconds(-1);
                    break;
                case ETimeType.去年:
                    startTime = new DateTime(date.Year, 1, 1).AddYears(-1);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.前年:
                    startTime = new DateTime(date.Year, 1, 1).AddYears(-2);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.前三天:
                    startTime = date.AddDays(-3);
                    endTime = date.AddMilliseconds(-1);
                    break;
                case ETimeType.明天:
                    startTime = date.AddDays(1);
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.后天:
                    startTime = date.AddDays(2);
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.下一周:
                    startTime = date.AddDays(1);
                    endTime = startTime.AddDays(7).AddMilliseconds(-1);
                    break;
                case ETimeType.下周:
                    startTime = date.AddDays(1 - day + 7);
                    endTime = startTime.AddDays(7).AddMilliseconds(-1);
                    break;
                case ETimeType.下一月:
                    startTime = date.AddDays(1);
                    endTime = startTime.AddMonths(1).AddMilliseconds(-1);
                    break;
                case ETimeType.下月:
                    startTime = new DateTime(date.Year, date.Month, 1).AddMonths(1);
                    endTime = startTime.AddMonths(1).AddMilliseconds(-1);
                    break;
                case ETimeType.下一年:
                    startTime = date.AddDays(1);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.明年:
                    startTime = new DateTime(date.Year, 1, 1).AddYears(1);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.后年:
                    startTime = new DateTime(date.Year, 1, 1).AddYears(2);
                    endTime = startTime.AddYears(1).AddMilliseconds(-1);
                    break;
                case ETimeType.后三天:
                    startTime = date.AddDays(1);
                    endTime = startTime.AddDays(3).AddMilliseconds(-1);
                    break;
                case ETimeType.所有:
                    startTime = new DateTime(1900, 1, 1);
                    endTime = new DateTime(5999, 1, 1);
                    break;
                case ETimeType.用户指定:
                    startTime = new DateTime(startTime.Year, startTime.Month, startTime.Day);
                    endTime = (new DateTime(endTime.Year, endTime.Month, endTime.Day)).AddDays(1).AddMilliseconds(-1);
                    break;
                case ETimeType.更早:
                    startTime = new DateTime(1900, 1, 1);
                    endTime = date.AddDays(-30).AddMilliseconds(-1);
                    break;
                case ETimeType.更晚:
                    startTime = date.AddDays(30);
                    endTime = new DateTime(5999, 1, 1);
                    break;
                default:
                    startTime = date;
                    endTime = startTime.AddDays(1).AddMilliseconds(-1);
                    break;
            }
        }
    }
}
