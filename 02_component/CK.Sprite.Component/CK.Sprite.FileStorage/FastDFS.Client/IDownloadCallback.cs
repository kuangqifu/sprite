﻿
namespace FastDFS.Client
{
   
    public interface IDownloadCallback
    {
        /// <summary>
        /// recv file content callback function, may be called more than once when the file downloaded
        /// </summary>
        /// <param name="fileSize">file size</param>
        /// <param name="data">data buff</param>
        /// <param name="bytes">data bytes</param>
        /// <returns>0 success, return none zero(errno) if fail</returns>
        int Recv(long fileSize, byte[] data, int bytes);
    }
}
