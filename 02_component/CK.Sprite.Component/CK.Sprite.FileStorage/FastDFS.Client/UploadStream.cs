﻿using System.IO;

namespace FastDFS.Client
{
    public class UploadStream:IUploadCallback
    {
        private Stream _inputStream; //input stream for reading
        private long _fileSize = 0;  //size of the uploaded file

        public UploadStream(Stream inputStream, long fileSize)
        {
            this._inputStream = inputStream;
            this._fileSize = fileSize;
        }

        public int Send(Stream output)
        {
            long remainBytes = _fileSize;
            byte[] buff = new byte[256 * 1024];
            int bytes;
            while (remainBytes > 0)
            {
                try
                {
                    if ((bytes = _inputStream.Read(buff, 0, remainBytes > buff.Length ? buff.Length : (int)remainBytes)) < 0)
                    {
                        return -1;
                    }
                }
                catch (IOException)
                {
                    return -1;
                }

                output.Write(buff, 0, bytes);
                remainBytes -= bytes;
            }

            return 0;
        }
    }
}
