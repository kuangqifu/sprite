﻿using System.Net;
using System.Net.Sockets;

namespace FastDFS.Client
{
    public class TrackerServer : FdfsServer
    {
        public TrackerServer(IPEndPoint ipEndPoint)
            : base(ipEndPoint)
        {
        }

        public TrackerServer(string ip, int port)
            : base(ip, port)
        {
        }

        /// <summary>
        /// 外网Ip映射处理
        /// </summary>
        public string StorageIpMaps { get; set; }
    }
}
