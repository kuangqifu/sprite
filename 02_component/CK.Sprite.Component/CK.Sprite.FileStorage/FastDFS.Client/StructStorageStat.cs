﻿using System;

namespace FastDFS.Client
{
    public class StructStorageStat : StructBase
    {
        protected static int FieldIndexStatus = 0;
        protected static int FieldIndexId = 1;
        protected static int FieldIndexIpAddr = 2;
        protected static int FieldIndexDomainName = 3;
        protected static int FieldIndexSrcIpAddr = 4;
        protected static int FieldIndexVersion = 5;
        protected static int FieldIndexJoinTime = 6;
        protected static int FieldIndexUpTime = 7;
        protected static int FieldIndexTotalMb = 8;
        protected static int FieldIndexFreeMb = 9;
        protected static int FieldIndexUploadPriority = 10;
        protected static int FieldIndexStorePathCount = 11;
        protected static int FieldIndexSubdirCountPerPath = 12;
        protected static int FieldIndexCurrentWritePath = 13;
        protected static int FieldIndexStoragePort = 14;
        protected static int FieldIndexStorageHttpPort = 15;

        protected static int FieldIndexConnectionAllocCount = 16;
        protected static int FieldIndexConnectionCurrentCount = 17;
        protected static int FieldIndexConnectionMaxCount = 18;

        protected static int FieldIndexTotalUploadCount = 19;
        protected static int FieldIndexSuccessUploadCount = 20;
        protected static int FieldIndexTotalAppendCount = 21;
        protected static int FieldIndexSuccessAppendCount = 22;
        protected static int FieldIndexTotalModifyCount = 23;
        protected static int FieldIndexSuccessModifyCount = 24;
        protected static int FieldIndexTotalTruncateCount = 25;
        protected static int FieldIndexSuccessTruncateCount = 26;
        protected static int FieldIndexTotalSetMetaCount = 27;
        protected static int FieldIndexSuccessSetMetaCount = 28;
        protected static int FieldIndexTotalDeleteCount = 29;
        protected static int FieldIndexSuccessDeleteCount = 30;
        protected static int FieldIndexTotalDownloadCount = 31;
        protected static int FieldIndexSuccessDownloadCount = 32;
        protected static int FieldIndexTotalGetMetaCount = 33;
        protected static int FieldIndexSuccessGetMetaCount = 34;
        protected static int FieldIndexTotalCreateLinkCount = 35;
        protected static int FieldIndexSuccessCreateLinkCount = 36;
        protected static int FieldIndexTotalDeleteLinkCount = 37;
        protected static int FieldIndexSuccessDeleteLinkCount = 38;
        protected static int FieldIndexTotalUploadBytes = 39;
        protected static int FieldIndexSuccessUploadBytes = 40;
        protected static int FieldIndexTotalAppendBytes = 41;
        protected static int FieldIndexSuccessAppendBytes = 42;
        protected static int FieldIndexTotalModifyBytes = 43;
        protected static int FieldIndexSuccessModifyBytes = 44;
        protected static int FieldIndexTotalDownloadBytes = 45;
        protected static int FieldIndexSuccessDownloadBytes = 46;
        protected static int FieldIndexTotalSyncInBytes = 47;
        protected static int FieldIndexSuccessSyncInBytes = 48;
        protected static int FieldIndexTotalSyncOutBytes = 49;
        protected static int FieldIndexSuccessSyncOutBytes = 50;
        protected static int FieldIndexTotalFileOpenCount = 51;
        protected static int FieldIndexSuccessFileOpenCount = 52;
        protected static int FieldIndexTotalFileReadCount = 53;
        protected static int FieldIndexSuccessFileReadCount = 54;
        protected static int FieldIndexTotalFileWriteCount = 55;
        protected static int FieldIndexSuccessFileWriteCount = 56;
        protected static int FieldIndexLastSourceUpdate = 57;
        protected static int FieldIndexLastSyncUpdate = 58;
        protected static int FieldIndexLastSyncedTimestamp = 59;
        protected static int FieldIndexLastHeartBeatTime = 60;
        protected static int FieldIndexIfTrunkFile = 61;

        protected static int FieldsTotalSize;
        protected static StructBase.FieldInfo[] FieldsArray = new StructBase.FieldInfo[62];

        static StructStorageStat()
        {
            int offset = 0;

            FieldsArray[FieldIndexStatus] = new StructBase.FieldInfo("status", offset, 1);
            offset += 1;

            FieldsArray[FieldIndexId] = new StructBase.FieldInfo("id", offset, ProtoCommon.FdfsStorageIdMaxSize);
            offset += ProtoCommon.FdfsStorageIdMaxSize;

            FieldsArray[FieldIndexIpAddr] = new StructBase.FieldInfo("ipAddr", offset, ProtoCommon.FdfsIpaddrSize);
            offset += ProtoCommon.FdfsIpaddrSize;

            FieldsArray[FieldIndexDomainName] = new StructBase.FieldInfo("domainName", offset, ProtoCommon.FdfsDomainNameMaxSize);
            offset += ProtoCommon.FdfsDomainNameMaxSize;

            FieldsArray[FieldIndexSrcIpAddr] = new StructBase.FieldInfo("srcIpAddr", offset, ProtoCommon.FdfsIpaddrSize);
            offset += ProtoCommon.FdfsIpaddrSize;

            FieldsArray[FieldIndexVersion] = new StructBase.FieldInfo("version", offset, ProtoCommon.FdfsVersionSize);
            offset += ProtoCommon.FdfsVersionSize;

            FieldsArray[FieldIndexJoinTime] = new StructBase.FieldInfo("joinTime", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexUpTime] = new StructBase.FieldInfo("upTime", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalMb] = new StructBase.FieldInfo("totalMB", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexFreeMb] = new StructBase.FieldInfo("freeMB", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexUploadPriority] = new StructBase.FieldInfo("uploadPriority", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStorePathCount] = new StructBase.FieldInfo("storePathCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSubdirCountPerPath] = new StructBase.FieldInfo("subdirCountPerPath", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexCurrentWritePath] = new StructBase.FieldInfo("currentWritePath", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStoragePort] = new StructBase.FieldInfo("storagePort", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStorageHttpPort] = new StructBase.FieldInfo("storageHttpPort", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexConnectionAllocCount] = new StructBase.FieldInfo("connectionAllocCount", offset, 4);
            offset += 4;

            FieldsArray[FieldIndexConnectionCurrentCount] = new StructBase.FieldInfo("connectionCurrentCount", offset, 4);
            offset += 4;

            FieldsArray[FieldIndexConnectionMaxCount] = new StructBase.FieldInfo("connectionMaxCount", offset, 4);
            offset += 4;

            FieldsArray[FieldIndexTotalUploadCount] = new StructBase.FieldInfo("totalUploadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessUploadCount] = new StructBase.FieldInfo("successUploadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalAppendCount] = new StructBase.FieldInfo("totalAppendCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessAppendCount] = new StructBase.FieldInfo("successAppendCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalModifyCount] = new StructBase.FieldInfo("totalModifyCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessModifyCount] = new StructBase.FieldInfo("successModifyCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalTruncateCount] = new StructBase.FieldInfo("totalTruncateCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessTruncateCount] = new StructBase.FieldInfo("successTruncateCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalSetMetaCount] = new StructBase.FieldInfo("totalSetMetaCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessSetMetaCount] = new StructBase.FieldInfo("successSetMetaCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalDeleteCount] = new StructBase.FieldInfo("totalDeleteCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessDeleteCount] = new StructBase.FieldInfo("successDeleteCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalDownloadCount] = new StructBase.FieldInfo("totalDownloadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessDownloadCount] = new StructBase.FieldInfo("successDownloadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalGetMetaCount] = new StructBase.FieldInfo("totalGetMetaCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessGetMetaCount] = new StructBase.FieldInfo("successGetMetaCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalCreateLinkCount] = new StructBase.FieldInfo("totalCreateLinkCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessCreateLinkCount] = new StructBase.FieldInfo("successCreateLinkCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalDeleteLinkCount] = new StructBase.FieldInfo("totalDeleteLinkCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessDeleteLinkCount] = new StructBase.FieldInfo("successDeleteLinkCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalUploadBytes] = new StructBase.FieldInfo("totalUploadBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessUploadBytes] = new StructBase.FieldInfo("successUploadBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalAppendBytes] = new StructBase.FieldInfo("totalAppendBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessAppendBytes] = new StructBase.FieldInfo("successAppendBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalModifyBytes] = new StructBase.FieldInfo("totalModifyBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessModifyBytes] = new StructBase.FieldInfo("successModifyBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalDownloadBytes] = new StructBase.FieldInfo("totalDownloadloadBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessDownloadBytes] = new StructBase.FieldInfo("successDownloadloadBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalSyncInBytes] = new StructBase.FieldInfo("totalSyncInBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessSyncInBytes] = new StructBase.FieldInfo("successSyncInBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalSyncOutBytes] = new StructBase.FieldInfo("totalSyncOutBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessSyncOutBytes] = new StructBase.FieldInfo("successSyncOutBytes", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalFileOpenCount] = new StructBase.FieldInfo("totalFileOpenCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessFileOpenCount] = new StructBase.FieldInfo("successFileOpenCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalFileReadCount] = new StructBase.FieldInfo("totalFileReadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessFileReadCount] = new StructBase.FieldInfo("successFileReadCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTotalFileWriteCount] = new StructBase.FieldInfo("totalFileWriteCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSuccessFileWriteCount] = new StructBase.FieldInfo("successFileWriteCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexLastSourceUpdate] = new StructBase.FieldInfo("lastSourceUpdate", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexLastSyncUpdate] = new StructBase.FieldInfo("lastSyncUpdate", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexLastSyncedTimestamp] = new StructBase.FieldInfo("lastSyncedTimestamp", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexLastHeartBeatTime] = new StructBase.FieldInfo("lastHeartBeatTime", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexIfTrunkFile] = new StructBase.FieldInfo("ifTrunkServer", offset, 1);
            offset += 1;

            FieldsTotalSize = offset;
        }
        public byte Status { get; private set; }
        public string Id { get; private set; }
        public string IpAddr { get; private set; }
        public string SrcIpAddr { get; private set; }
        public string DomainName { get; private set; }
        public string Version { get; private set; }
        public long TotalMb { get; private set; }
        public long FreeMb { get; private set; }
        public int UploadPriority { get; private set; }
        public DateTime JoinTime { get; private set; }
        public DateTime UpTime { get; private set; }
        public int StorePathCount { get; private set; }
        public int SubdirCountPerPath { get; private set; }
        public int StoragePort { get; private set; }
        public int StorageHttpPort { get; private set; }
        public int CurrentWritePath { get; private set; }
        public int ConnectionAllocCount { get; private set; }
        public int ConnectionCurrentCount { get; private set; }
        public int ConnectionMaxCount { get; private set; }
        public long TotalUploadCount { get; private set; }
        public long SuccessUploadCount { get; private set; }
        public long TotalAppendCount { get; private set; }
        public long SuccessAppendCount { get; private set; }
        public long TotalModifyCount { get; private set; }
        public long SuccessModifyCount { get; private set; }
        public long TotalTruncateCount { get; private set; }
        public long SuccessTruncateCount { get; private set; }
        public long TotalSetMetaCount { get; private set; }
        public long SuccessSetMetaCount { get; private set; }
        public long TotalDeleteCount { get; private set; }
        public long SuccessDeleteCount { get; private set; }
        public long TotalDownloadCount { get; private set; }
        public long SuccessDownloadCount { get; private set; }
        public long TotalGetMetaCount { get; private set; }
        public long SuccessGetMetaCount { get; private set; }
        public long TotalCreateLinkCount { get; private set; }
        public long SuccessCreateLinkCount { get; private set; }
        public long TotalDeleteLinkCount { get; private set; }
        public long SuccessDeleteLinkCount { get; private set; }
        public long TotalUploadBytes { get; private set; }
        public long SuccessUploadBytes { get; private set; }
        public long TotalAppendBytes { get; private set; }
        public long SuccessAppendBytes { get; private set; }
        public long TotalModifyBytes { get; private set; }
        public long SuccessModifyBytes { get; private set; }
        public long TotalDownloadloadBytes { get; private set; }
        public long SuccessDownloadloadBytes { get; private set; }
        public long TotalSyncInBytes { get; private set; }
        public long SuccessSyncInBytes { get; private set; }
        public long TotalSyncOutBytes { get; private set; }
        public long SuccessSyncOutBytes { get; private set; }
        public long TotalFileOpenCount { get; private set; }
        public long SuccessFileOpenCount { get; private set; }
        public long TotalFileReadCount { get; private set; }
        public long SuccessFileReadCount { get; private set; }
        public long TotalFileWriteCount { get; private set; }
        public long SuccessFileWriteCount { get; private set; }
        public DateTime LastSourceUpdate { get; private set; }
        public DateTime LastSyncUpdate { get; private set; }
        public DateTime LastSyncedTimestamp { get; private set; }
        public DateTime LastHeartBeatTime { get; private set; }
        public bool IfTrunkServer { get; private set; }


        public override void SetFields(byte[] bs, int offset)
        {
            this.Status = ByteValue(bs, offset, FieldsArray[FieldIndexStatus]);
            this.Id = StringValue(bs, offset, FieldsArray[FieldIndexId]);
            this.IpAddr = StringValue(bs, offset, FieldsArray[FieldIndexIpAddr]);
            this.SrcIpAddr = StringValue(bs, offset, FieldsArray[FieldIndexSrcIpAddr]);
            this.DomainName = StringValue(bs, offset, FieldsArray[FieldIndexDomainName]);
            this.Version = StringValue(bs, offset, FieldsArray[FieldIndexVersion]);
            this.TotalMb = LongValue(bs, offset, FieldsArray[FieldIndexTotalMb]);
            this.FreeMb = LongValue(bs, offset, FieldsArray[FieldIndexFreeMb]);
            this.UploadPriority = IntValue(bs, offset, FieldsArray[FieldIndexUploadPriority]);
            this.JoinTime = DateValue(bs, offset, FieldsArray[FieldIndexJoinTime]);
            this.UpTime = DateValue(bs, offset, FieldsArray[FieldIndexUpTime]);
            this.StorePathCount = IntValue(bs, offset, FieldsArray[FieldIndexStorePathCount]);
            this.SubdirCountPerPath = IntValue(bs, offset, FieldsArray[FieldIndexSubdirCountPerPath]);
            this.StoragePort = IntValue(bs, offset, FieldsArray[FieldIndexStoragePort]);
            this.StorageHttpPort = IntValue(bs, offset, FieldsArray[FieldIndexStorageHttpPort]);
            this.CurrentWritePath = IntValue(bs, offset, FieldsArray[FieldIndexCurrentWritePath]);
            this.ConnectionAllocCount = Int32Value(bs, offset, FieldsArray[FieldIndexConnectionAllocCount]);
            this.ConnectionCurrentCount = Int32Value(bs, offset, FieldsArray[FieldIndexConnectionCurrentCount]);
            this.ConnectionMaxCount = Int32Value(bs, offset, FieldsArray[FieldIndexConnectionMaxCount]);
            this.TotalUploadCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalUploadCount]);
            this.SuccessUploadCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessUploadCount]);
            this.TotalAppendCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalAppendCount]);
            this.SuccessAppendCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessAppendCount]);
            this.TotalModifyCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalModifyCount]);
            this.SuccessModifyCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessModifyCount]);
            this.TotalTruncateCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalTruncateCount]);
            this.SuccessTruncateCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessTruncateCount]);
            this.TotalSetMetaCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalSetMetaCount]);
            this.SuccessSetMetaCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessSetMetaCount]);
            this.TotalDeleteCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalDeleteCount]);
            this.SuccessDeleteCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessDeleteCount]);
            this.TotalDownloadCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalDownloadCount]);
            this.SuccessDownloadCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessDownloadCount]);
            this.TotalGetMetaCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalGetMetaCount]);
            this.SuccessGetMetaCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessGetMetaCount]);
            this.TotalCreateLinkCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalCreateLinkCount]);
            this.SuccessCreateLinkCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessCreateLinkCount]);
            this.TotalDeleteLinkCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalDeleteLinkCount]);
            this.SuccessDeleteLinkCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessDeleteLinkCount]);
            this.TotalUploadBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalUploadBytes]);
            this.SuccessUploadBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessUploadBytes]);
            this.TotalAppendBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalAppendBytes]);
            this.SuccessAppendBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessAppendBytes]);
            this.TotalModifyBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalModifyBytes]);
            this.SuccessModifyBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessModifyBytes]);
            this.TotalDownloadloadBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalDownloadBytes]);
            this.SuccessDownloadloadBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessDownloadBytes]);
            this.TotalSyncInBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalSyncInBytes]);
            this.SuccessSyncInBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessSyncInBytes]);
            this.TotalSyncOutBytes = LongValue(bs, offset, FieldsArray[FieldIndexTotalSyncOutBytes]);
            this.SuccessSyncOutBytes = LongValue(bs, offset, FieldsArray[FieldIndexSuccessSyncOutBytes]);
            this.TotalFileOpenCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalFileOpenCount]);
            this.SuccessFileOpenCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessFileOpenCount]);
            this.TotalFileReadCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalFileReadCount]);
            this.SuccessFileReadCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessFileReadCount]);
            this.TotalFileWriteCount = LongValue(bs, offset, FieldsArray[FieldIndexTotalFileWriteCount]);
            this.SuccessFileWriteCount = LongValue(bs, offset, FieldsArray[FieldIndexSuccessFileWriteCount]);
            this.LastSourceUpdate = DateValue(bs, offset, FieldsArray[FieldIndexLastSourceUpdate]);
            this.LastSyncUpdate = DateValue(bs, offset, FieldsArray[FieldIndexLastSyncUpdate]);
            this.LastSyncedTimestamp = DateValue(bs, offset, FieldsArray[FieldIndexLastSyncedTimestamp]);
            this.LastHeartBeatTime = DateValue(bs, offset, FieldsArray[FieldIndexLastHeartBeatTime]);
            this.IfTrunkServer = BooleanValue(bs, offset, FieldsArray[FieldIndexIfTrunkFile]);

        }

        public static int GetFieldsTotalSize()
        {
            return FieldsTotalSize;
        }
    }
}
