﻿namespace FastDFS.Client
{
    //
    // 摘要:
    //     支持克隆，即用与现有实例相同的值创建类的新实例。
    public interface ICloneable
    {
        //
        // 摘要:
        //     创建作为当前实例副本的新对象。
        //
        // 返回结果:
        //     作为此实例副本的新对象。
        object Clone();
    }
}
