﻿
namespace FastDFS.Client
{
    public class StructGroupStat : StructBase
    {
        protected static int FieldIndexGroupName = 0;
        protected static int FieldIndexTotalMb = 1;
        protected static int FieldIndexFreeMb = 2;
        protected static int FieldIndexTrunkFreeMb = 3;
        protected static int FieldIndexStorageCount = 4;
        protected static int FieldIndexStoragePort = 5;
        protected static int FieldIndexStorageHttpPort = 6;
        protected static int FieldIndexActiveCount = 7;
        protected static int FieldIndexCurrentWriteServer = 8;
        protected static int FieldIndexStorePathCount = 9;
        protected static int FieldIndexSubdirCountPerPath = 10;
        protected static int FieldIndexCurrentTrunkFileId = 11;

        protected static int FieldsTotalSize;
        protected static StructBase.FieldInfo[] FieldsArray = new StructBase.FieldInfo[12];

        static StructGroupStat()
        {
            int offset = 0;
            FieldsArray[FieldIndexGroupName] = new StructBase.FieldInfo("groupName", offset, ProtoCommon.FdfsGroupNameMaxLen + 1);
            offset += ProtoCommon.FdfsGroupNameMaxLen + 1;

            FieldsArray[FieldIndexTotalMb] = new StructBase.FieldInfo("totalMB", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexFreeMb] = new StructBase.FieldInfo("freeMB", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexTrunkFreeMb] = new StructBase.FieldInfo("trunkFreeMB", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStorageCount] = new StructBase.FieldInfo("storageCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStoragePort] = new StructBase.FieldInfo("storagePort", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStorageHttpPort] = new StructBase.FieldInfo("storageHttpPort", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexActiveCount] = new StructBase.FieldInfo("activeCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexCurrentWriteServer] = new StructBase.FieldInfo("currentWriteServer", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexStorePathCount] = new StructBase.FieldInfo("storePathCount", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexSubdirCountPerPath] = new StructBase.FieldInfo("subdirCountPerPath", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsArray[FieldIndexCurrentTrunkFileId] = new StructBase.FieldInfo("currentTrunkFileId", offset, ProtoCommon.FdfsProtoPkgLenSize);
            offset += ProtoCommon.FdfsProtoPkgLenSize;

            FieldsTotalSize = offset;
        }
        
        /// <summary>
        /// name of this group
        /// </summary>
        public string GroupName { get; private set; }
        /// <summary>
        /// total disk storage in MB
        /// </summary>
        public long TotalMb { get; private set; }
        /// <summary>
        /// free disk space in MB
        /// </summary>
        public long FreeMb { get; private set; }
        /// <summary>
        /// trunk free space in MB
        /// </summary>
        public long TrunkFreeMb { get; private set; }
        public int StorageCount { get; private set; }
        public int StoragePort { get; private set; }
        public int StorageHttpPort { get; private set; }
        public int ActiveCount { get; private set; }
        public int CurrentWriteServer { get; private set; }
        public int StorePathCount { get; private set; }
        public int SubdirCountPerPath { get; private set; }
        public int CurrentTrunkFileId { get; private set; }

        public override void SetFields(byte[] bs, int offset)
        {
            this.GroupName = StringValue(bs, offset, FieldsArray[FieldIndexGroupName]);
            this.TotalMb = LongValue(bs, offset, FieldsArray[FieldIndexTotalMb]);
            this.FreeMb = LongValue(bs, offset, FieldsArray[FieldIndexFreeMb]);
            this.TrunkFreeMb = LongValue(bs, offset, FieldsArray[FieldIndexTrunkFreeMb]);
            this.StorageCount = IntValue(bs, offset, FieldsArray[FieldIndexStorageCount]);
            this.StoragePort = IntValue(bs, offset, FieldsArray[FieldIndexStoragePort]);
            this.StorageHttpPort = IntValue(bs, offset, FieldsArray[FieldIndexStorageHttpPort]);
            this.ActiveCount = IntValue(bs, offset, FieldsArray[FieldIndexActiveCount]);
            this.CurrentWriteServer = IntValue(bs, offset, FieldsArray[FieldIndexCurrentWriteServer]);
            this.StorePathCount = IntValue(bs, offset, FieldsArray[FieldIndexStorePathCount]);
            this.SubdirCountPerPath = IntValue(bs, offset, FieldsArray[FieldIndexSubdirCountPerPath]);
            this.CurrentTrunkFileId = IntValue(bs, offset, FieldsArray[FieldIndexCurrentTrunkFileId]);
        }

        public static int GetFieldsTotalSize()
        {
            return FieldsTotalSize;
        }
    }
}
