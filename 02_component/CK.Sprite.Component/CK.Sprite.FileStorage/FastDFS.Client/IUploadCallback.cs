﻿using System.IO;

namespace FastDFS.Client
{
    public interface IUploadCallback
    {
        /// <summary>
        /// send file content callback function, be called only once when the file uploaded
        /// </summary>
        /// <param name="output">output stream for writing file content</param>
        /// <returns>0 success, return none zero(errno) if fail</returns>
        int Send(Stream output);
    }
}
