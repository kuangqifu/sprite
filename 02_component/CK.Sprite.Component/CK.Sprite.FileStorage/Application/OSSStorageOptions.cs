﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CK.Sprite.FileStorage.Application
{
    public class OSSStorageOptions: FileStorgeOptions
    {

        /// <summary>
        /// OSS 服务地址
        /// </summary>
        public string ServerAddress { get; set; }
        /// <summary>
        /// OSS key
        /// </summary>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// OSS secret
        /// </summary>
        public string AccessKeySecret { get; set; }

        /// <summary>
        /// 阿里云OSS bucket
        /// </summary>
        public string Bucket { get; set; }
    }
}
