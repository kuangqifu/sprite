﻿using CK.Sprite.FileStorage.Application;

namespace CK.Sprite.FileStorage
{
    /// <summary>
    /// configurations of FastDFS client
    /// </summary>
    public class FastDfsOptions: FileStorgeOptions
    {
        /// <summary>
        /// all tracker ip(s) and port
        /// </summary>
        public string[] Trackers { get; set; }

        /// <summary>
        /// wether to enable local storage provider
        /// </summary>
        public bool EnableLocalStorageProvider { get; set; } = false;

        /// <summary>
        /// 处理外网映射问题，格式为"OldIp1:OldPort1#NewIp1:NewPort1;OldIp2:OldPort2#NewIp2:NewPort2"
        /// </summary>
        public string Bucket { get; set; }
    }
}