﻿using System;

namespace CK.Sprite.FileStorage
{

    public class UploadFileInfo
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件访问路径
        /// </summary>
        public string ServerFilePath { get; set; }

        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UploadTime { get; set; }

        /// <summary>
        /// 文件大小(bit)
        /// </summary>
        public long? FileSize { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public string FileExt { get; set; }
    }
}