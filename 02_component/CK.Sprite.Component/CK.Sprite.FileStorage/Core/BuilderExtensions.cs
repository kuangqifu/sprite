﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Builder;
using System.Net.WebSockets;

namespace CK.Sprite.FileStorage
{
	public static class BuilderExtensions
	{
        public static IApplicationBuilder UseFileStorage(this IApplicationBuilder app, ILogHelper logHelper)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            LogHelper.InitLogHelper(logHelper);

            var router = new FileStorageClientHttpRouter();
            app.Map("/file.upload", uploadApp =>
            {
                uploadApp.UseRouter(router);
            });

            var fileStoreManager = app.ApplicationServices.GetService<IFileStoreManager>();
            FileStorageHelper.SetFileStoreManager(fileStoreManager);

            return app;
        }
    }
}