﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CK.Sprite.FileStorage
{
    public class FileStorageClientHttpRouter : IRouter
    {
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            return null;
        }

        /// <summary>
		/// Takes a route/http contexts and attempts to parse, invoke, respond to an Rpc request
		/// </summary>
		/// <param name="context">Route context</param>
		/// <returns>Task for async routing</returns>
		public async Task RouteAsync(RouteContext context)
        {
            try
            {
                await FileStorageExtension.UploadAsync(context.HttpContext);

                context.MarkAsHandled();

            }
            catch (Exception ex)
            {
                context.MarkAsHandled();
            }
        }
    }

    public static class RouteContextExtensions
    {
        public static void MarkAsHandled(this RouteContext context)
        {
            context.Handler = c => Task.FromResult(0);
        }
    }
}
