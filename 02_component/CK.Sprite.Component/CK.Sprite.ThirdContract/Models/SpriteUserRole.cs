﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 用户角色信息
    /// </summary>
    public class SpriteUserRole
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleId { get; set; }
    }
}
