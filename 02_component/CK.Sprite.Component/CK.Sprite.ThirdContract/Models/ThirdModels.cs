﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class GetThirdUserInput
    {
        /// <summary>
        /// 限制的用户信息
        /// </summary>
        public string LimitUserId { get; set; }

        /// <summary>
        /// 用户Id集合
        /// </summary>
        public List<string> UserIds { get; set; }

        /// <summary>
        /// 部门角色信息
        /// </summary>
        public List<GetThirdUserRoleDetailInput> ThirdUserRoleDetailInputs { get; set; }
    }

    public class GetThirdUserRoleDetailInput
    {
        /// <summary>
        /// 部门Id：为空，表示不限制；-1表示本级部门；-2表示上级部门；其他为正常部门Id
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        public string RoleId { get; set; }
    }
}
