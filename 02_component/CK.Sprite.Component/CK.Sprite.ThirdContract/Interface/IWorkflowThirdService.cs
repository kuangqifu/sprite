﻿using CK.Sprite.Framework;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface IWorkflowThirdService
    {
        /// <summary>
        /// 获取表单关联流程信息
        /// </summary>
        /// <param name="instanceIds">流程实例Id集合</param>
        /// <returns></returns>
        Task<List<FormWorkflowInfo>> GetFormWorkflowInfos(List<string> instanceIds);

        /// <summary>
        /// 根据表单Id获取打开流程信息
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="workflowInfoType">类型参数</param>
        /// <returns></returns>
        Task<object> GetFormOpenWorkflowData(string id, EGetWorkflowInfoType workflowInfoType);

        /// <summary>
        /// 根据表单Id获取打开父流程信息
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="workflowInfoType">类型参数</param>
        /// <returns></returns>
        Task<object> GetParentFormOpenWorkflowData(string id, EGetWorkflowInfoType workflowInfoType);

        /// <summary>
        /// 获取父流程表单Id
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="workflowInfoType">类型参数</param>
        /// <returns></returns>
        Task<string> GetParentFormId(string id, EGetWorkflowInfoType workflowInfoType);
    }

    public enum EGetWorkflowInfoType
    {
        FormId = 1,
        InstanceId = 2
    }
}
