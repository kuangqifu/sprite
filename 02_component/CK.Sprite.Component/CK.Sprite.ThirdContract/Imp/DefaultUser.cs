﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 当前租户信息
    /// </summary>
    public class DefaultCurrentUser : ICurrentUser
    {
        public string UserId => "adminId";

        public string UserName => "admin";

        public string DeptId => "0";

        public string Name => "管理员";

        public string[] Roles => new string[] { "admin" };
    }
}
