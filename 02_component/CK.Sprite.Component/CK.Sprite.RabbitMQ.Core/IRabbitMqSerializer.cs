﻿using System;

namespace CK.Sprite.RabbitMQ.Core
{
    public interface IRabbitMqSerializer
    {
        byte[] Serialize(object obj);

        T Deserialize<T>(byte[] value);
    }
}
