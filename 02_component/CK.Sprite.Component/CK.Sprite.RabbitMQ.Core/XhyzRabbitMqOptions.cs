﻿namespace CK.Sprite.RabbitMQ.Core
{
    public class SpriteRabbitMqOptions
    {
        public RabbitMqConnections Connections { get; }

        public SpriteRabbitMqOptions()
        {
            Connections = new RabbitMqConnections();
        }
    }
}
