﻿using System;
using System.Collections.Generic;

namespace CK.Sprite.RabbitMQ.Service
{
    public class CommonRabbitMqOptions
    {
        public Dictionary<string, Type> CommonQueueTypes { get; }

        public Dictionary<string, CommonQueueConfiguration> CommonQueueConfigs { get; }

        /// <summary>
        /// Default value: "XhyzMq."
        /// </summary>
        public string DefaultQueueNamePrefix { get; set; }

        public CommonRabbitMqOptions()
        {
            CommonQueueConfigs = new Dictionary<string, CommonQueueConfiguration>();
            CommonQueueTypes = new Dictionary<string, Type>();
            DefaultQueueNamePrefix = "XhyzMq.";
        }

        public void AddToMq(string queueName, Type queueType, CommonQueueConfiguration commonQueueConfiguration)
        {
            if(!CommonQueueTypes.ContainsKey(queueName))
            {
                CommonQueueTypes.Add(queueName, queueType);
                CommonQueueConfigs.Add(queueName, commonQueueConfiguration);
            }
        }
    }
}
