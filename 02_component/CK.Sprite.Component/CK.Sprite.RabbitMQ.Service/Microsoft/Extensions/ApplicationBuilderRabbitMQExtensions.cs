﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using CK.Sprite.RabbitMQ.Core;
using CK.Sprite.RabbitMQ.Service;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ApplicationBuilderRabbitMQExtensions
    {
        public static void UseRabbitMQCore(this IApplicationBuilder app)
        {
            StartJobQueueManager(app);
        }

        private static void StartJobQueueManager(IApplicationBuilder app)
        {
            AsyncHelper.RunSync(
                () => app.ApplicationServices
                    .GetRequiredService<ICommonQueueManager>()
                    .StartAsync()
            );
        }
    }
}
