﻿using System.Threading.Tasks;
using CK.Sprite.RabbitMQ.Core;

namespace CK.Sprite.RabbitMQ.Service
{
    public interface ICommonQueueManager : IRunnable
    {
        Task<ICommonQueue> GetAsync(string queueName);
    }
}