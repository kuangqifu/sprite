﻿using System.Collections.Generic;

namespace CK.Sprite.Cache
{
    public static class ReceiveCacheNotice
    {
        public static void ReceiveClearCache(string key)
        {
            LocalCacheContainer.ClearCache(key);
        }

        public static void ReceiveClearCaches(List<string> keys)
        {
            foreach(var key in keys)
            {
                LocalCacheContainer.ClearCache(key);
            }
        }

        public static void SetLocalCacheIsEnabled(bool isEnabled)
        {
            LocalCacheContainer.SetLocalCacheIsEnabled(isEnabled);
        }
    }
}
