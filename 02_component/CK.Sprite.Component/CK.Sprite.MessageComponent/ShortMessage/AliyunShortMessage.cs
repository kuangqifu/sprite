﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CK.Sprite.MessageComponent
{
    public class AliyunShortMessage : IAliyunShortMessage
    {
        private readonly AliyunConfig _aliyunConfig;
        public AliyunShortMessage(IOptions<AliyunConfig> aliyunConfig)
        {
            _aliyunConfig = aliyunConfig.Value;
        }

        public AlyNoteResponse SendCommonMessage(string phone, Dictionary<string, string> templateParam, string sortMessageTemplateCode)
        {
            List<string> phoneNumbers = new List<string>() { phone };
            Random random = new Random();
            var resultData = DoSendMessage(phoneNumbers, templateParam, sortMessageTemplateCode);
            return resultData;
        }

        public AlyNoteResponse SendShortMessage(string phone, string code)
        {
            List<string> phoneNumbers = new List<string>() { phone };
            Dictionary<string, string> templateParam = new Dictionary<string, string>();
            Random random = new Random();
            templateParam.Add("code", code);
            var resultData = DoSendMessage(phoneNumbers, templateParam, _aliyunConfig.MessageTemplateCode);
            if (resultData.IsSuccess)
            {
                resultData.Data = code;
            }
            return resultData;
        }

        private AlyNoteResponse DoSendMessage(List<string> phoneNumbers, Dictionary<string, string> templateParam, string templateCode)
        {
            string regionId = "cn-hangzhou";
            IClientProfile profile = DefaultProfile.GetProfile(regionId, _aliyunConfig.AccessKeyID, _aliyunConfig.AccessKeySecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            #region 固定参数
            request.Method = MethodType.POST;
            request.Domain = "dysmsapi.aliyuncs.com";
            request.Version = "2017-05-25";
            request.Action = "SendSms";
            #endregion

            #region 请求参数
            var strPhoneNumbers = string.Join(",", phoneNumbers);
            var strTemplateParam = JsonConvert.SerializeObject(templateParam);
            request.AddQueryParameters("SignName", _aliyunConfig.SignName);
            request.AddQueryParameters("PhoneNumbers", strPhoneNumbers);
            request.AddQueryParameters("TemplateCode", templateCode);
            request.AddQueryParameters("TemplateParam", strTemplateParam);
            #endregion
            try
            {
                CommonResponse response = client.GetCommonResponse(request);
                var responseData = JsonConvert.DeserializeObject<AlyNoteResponse>(response.Data);
                if ("ok".Equals(responseData.Code.ToLower()))
                {
                    responseData.IsSuccess = true;
                }
                return responseData;
            }
            catch (ServerException e)
            {
                return new AlyNoteResponse() { Message = e.Message };
            }
            catch (ClientException e)
            {
                return new AlyNoteResponse() { Message = e.Message };
            }
        }
    }

    public class AlyNoteResponse<T>
    {
        public bool IsSuccess { get; set; }
        public T Data { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string RequestId { get; set; }
        public string BizId { get; set; }
    }
    public class AlyNoteResponse : AlyNoteResponse<string>
    {

    }
}
